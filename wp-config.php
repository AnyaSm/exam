<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'exam');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`~di%x=rx0]Kd.hviL zM*ex9qORJz|JJc[Z}KG<G!Dx?TM!];P!B[fE/j#k-o `');
define('SECURE_AUTH_KEY',  '5F&7c;*T,g+mE4EiPYayNHBv;gTI,aR5)*pxJ7EJMG]_{%TQ$C~$X^L4Pas]`]Vl');
define('LOGGED_IN_KEY',    '.?yHlR|p[DuB(SF)I(IN<!EOs8eAd602!w^xVvwSg-dGt7T:d#-`.R>cO?,) JA+');
define('NONCE_KEY',        '{$!^FAb<@y*mDq6W+HWFuN?t4d,nXX&_!*Zu!0T;^vSA`6|iqVz@M2(yidISD{?]');
define('AUTH_SALT',        'wPGaF++8|Xae5Ul6&D[2[BlYk+Pqy6Q8m{:, 3-H[IQGQ3bq?IN9JaHzKqe|,Gtu');
define('SECURE_AUTH_SALT', '|.e}ZW(nKL6uTe*%4}R4)#<p*,,$K gF.Lu/uV2 pJ9BV|wp3Md6eTCU3opu:M/a');
define('LOGGED_IN_SALT',   '7+Fjhf=4hw&/><D^(HO+M[8=f&?7Zf+jgUdvNF -UQ*u8-*w4ZrA05bAUTD#EV7}');
define('NONCE_SALT',       'j.4K;W]8fVk*DqShxX~RVaw`o:NE,?e(Cv~NqY[Hm1#=s>L IF)%(=.aqE9Z`A)n');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
