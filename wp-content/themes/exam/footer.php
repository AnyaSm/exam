    <footer class="footer col-xs-12">
        <div class="container">
            <div class="information col-sm-4">
                <h2>
                    <?php if ( function_exists( 'jetpack_the_site_logo' ) ) jetpack_the_site_logo(); ?>
                </h2>
                <ul>
                    <?php if(!dynamic_sidebar('footer-sidebar')) : ?>

                    <?php endif; ?>
                </ul>
                <div class="footer-social">
                    <a class="facebook" href="<?php echo get_theme_mod('social_links_facebook'); ?>">
                        <span class="fa fa-facebook"></span>
                    </a>
                    <a class="twitter" href="<?php echo get_theme_mod('social_links_twitter'); ?>">
                        <span class="fa fa-twitter"></span>
                    </a>
                    <a class="google-plus" href="<?php echo get_theme_mod('social_links_google'); ?>">
                        <span class="fa fa-google-plus"></span>
                    </a>
                    <a class="linkedin" href="<?php echo get_theme_mod('social_links_instagram'); ?>">
                        <span class="fa fa-linkedin"></span>
                    </a>
                </div>
            </div>
            <div class="footer-menu col-sm-4">
                <h2>Navigation</h2>
                <?php wp_nav_menu( array(
                    'theme_location'  => 'foot-nav',
                    'container'       => false,
                    'menu_class'      => 'column-xs start-xs'
                ));
                ?>
            </div>
            <div class="contact-form col-sm-4">
                <?php echo do_shortcode ('[contact-form-7 id="52" title="Contact"]'); ?>
            </div>
        </div>
    </footer>


<?php wp_footer(); ?>

</body>
</html>