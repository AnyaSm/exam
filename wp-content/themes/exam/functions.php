<?php

function load_style() {
    wp_enqueue_style('bootstrap.css', get_template_directory_uri() . '/styles/css/bootstrap.css');
    wp_enqueue_style('font-awesome.css', get_template_directory_uri() . '/styles/css/font-awesome.css');
    wp_enqueue_style('flexboxgrid.min.css', get_template_directory_uri() . '/styles/flexboxgrid/css/flexboxgrid.min.css');
    wp_enqueue_style('hover.css', get_template_directory_uri() . '/styles/css/hover.css');
    wp_enqueue_style('main', get_template_directory_uri() . '/styles/css/styles.css');

}

add_action ('wp_enqueue_scripts', 'load_style');

function load_script() {
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js');
    wp_enqueue_script('main.js', get_template_directory_uri() . '/js/main.js');
}

add_action ('wp_enqueue_scripts', 'load_script');

function my_scripts_method() {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js');
    wp_enqueue_script( 'jquery' );
}

add_action( 'wp_enqueue_scripts', 'my_scripts_method' );

// Navigation Menus
register_nav_menu ('main-nav', 'header-menu');
// Navigation Menus
register_nav_menu ('foot-nav', 'footer-menu');

// LOGO
function my_after_setup_theme() {
    add_image_size( 'my-theme-logo-size', auto, auto, true );
    add_theme_support( 'site-logo', array( 'size' => 'my-theme-logo-size' ) );
}
add_action( 'after_setup_theme', 'my_after_setup_theme' );

// поддержка миниаютр
add_theme_support('post-thumbnails');

//content
function content($limit) {
    $content = explode(' ', get_the_content(), $limit);
    if (count($content)>=$limit) {
        array_pop($content);
        $content = implode(" ",$content).'...';
    } else {
        $content = implode(" ",$content);
    }
    $content = preg_replace('/\[.+\]/','', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content; }

/**
 * Register our sidebars and widgetized areas.
 *
 */
register_sidebar( array(
    'name'          => 'sidebar',
    'id'            => 'main-sidebar',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>',
    'description'   => 'create widgets here'
) );

register_sidebar( array(
    'name'          => 'foot-sidebar',
    'id'            => 'footer-sidebar',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>',
    'description'   => 'create widgets here'
) );


function is_type_page() { // Check if the current post is a page
    global $post;

    if ($post->post_type == 'page') {
        return true;
    } else {
        return false;
    }
}

//Popular post
function setPostViews($postID) {
    $count_key = post_views_count;
    $count = get_post_meta($postID, $count_key, true);
if($count==»){
    $count = 0;
    delete_post_meta($postID, $count_key);
    add_post_meta($postID, $count_key, 0);
}else{
    $count++;
    update_post_meta($postID, $count_key, $count);
}
}
function getPostViews($postID)
{
    $count_key = post_views_count;
$count = get_post_meta($postID, $count_key, true);
if ($count == ») {
    delete_post_meta($postID, $count_key);
    add_post_meta($postID, $count_key, 0);
return "0";
}
return $count;
}

//field
function hw_customize_register( $wp_customize ) {
    //All our sections, settings, and controls will be added here
    $wp_customize->add_section( 'hw_social_links' , array(
        'title'      => __( 'Social links', 'hw' ),
        'priority'   => 30,
    ) );

    $wp_customize->add_setting( 'social_links_facebook' , array(
        'default'     => '',
        'transport'   => 'refresh',
    ) );

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'social_links_facebook', array(
        'label'        => __( 'Facebook', 'hw' ),
        'section'    => 'hw_social_links',
        'settings'   => 'social_links_facebook',
    ) ) );

    $wp_customize->add_setting( 'social_links_twitter' , array(
        'default'     => '',
        'transport'   => 'refresh',
    ) );

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'social_links_twitter', array(
        'label'        => __( 'Twitter', 'hw' ),
        'section'    => 'hw_social_links',
        'settings'   => 'social_links_twitter',
    ) ) );

    $wp_customize->add_setting( 'social_links_google' , array(
        'default'     => '',
        'transport'   => 'refresh',
    ) );

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'social_links_google', array(
        'label'        => __( 'Google', 'hw' ),
        'section'    => 'hw_social_links',
        'settings'   => 'social_links_google',
    ) ) );

    $wp_customize->add_setting( 'social_links_youtube' , array(
        'default'     => '',
        'transport'   => 'refresh',
    ) );

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'social_links_youtube', array(
        'label'        => __( 'Youtube', 'hw' ),
        'section'    => 'hw_social_links',
        'settings'   => 'social_links_youtube',
    ) ) );

    $wp_customize->add_setting( 'social_links_instagram' , array(
        'default'     => '',
        'transport'   => 'refresh',
    ) );

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'social_links_instagram', array(
        'label'        => __( 'Instagram', 'hw' ),
        'section'    => 'hw_social_links',
        'settings'   => 'social_links_instagram',
    ) ) );

    $wp_customize->add_setting( 'social_links_dribbble' , array(
        'default'     => '',
        'transport'   => 'refresh',
    ) );

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'social_links_dribbble', array(
        'label'        => __( 'Dribbble', 'hw' ),
        'section'    => 'hw_social_links',
        'settings'   => 'social_links_dribbble',
    ) ) );

    $wp_customize->add_setting( 'social_links_pinterest' , array(
        'default'     => '',
        'transport'   => 'refresh',
    ) );

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'social_links_pinterest', array(
        'label'        => __( 'Pinterest', 'hw' ),
        'section'    => 'hw_social_links',
        'settings'   => 'social_links_pinterest',
    ) ) );

    $wp_customize->add_setting( 'social_links_color' , array(
        'default'     => '',
        'transport'   => 'refresh',
    ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'social_links_color', array(
        'label'        => __( 'Social links background color', 'hw' ),
        'section'    => 'hw_social_links',
        'settings'   => 'social_links_color',
    ) ) );
}
add_action( 'customize_register', 'hw_customize_register' );


add_filter('wp_list_categories', 'cat_count_span');
function cat_count_span($links) {
    $links = str_replace('</a> (', '</a> <span>(', $links);
    $links = str_replace(')', ')</span>', $links);
    return $links;
}

// Creates Movie Reviews Custom Post Type
function services_reviews_init() {
    $args = array(
        'label' => 'Services',
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'services-reviews'),
        'query_var' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes',)
    );
    register_post_type( 'services-reviews', $args );
}
add_action( 'init', 'services_reviews_init' );

/**
 * Filter the except length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 10;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );



function maks_filter_terms( $exclusions, $args ){
// Идентификаторы, которые будут исключены
    $exclude = "3, 5, 6"; // Изменить эти идентификаторы
// Генерация кода исключения SQL
    $exterms = wp_parse_id_list( $exclude );
    foreach ( $exterms as $exterm ) {
        if ( empty($exclusions) )
            $exclusions = ' AND ( t.term_id <> ' . intval($exterm) . ' ';
        else
            $exclusions .= ' AND t.term_id <> ' . intval($exterm) . ' ';
    }

    if ( !empty($exclusions) )
        $exclusions .= ')';

    return $exclusions;
}

// Наконец добавить наш фильтр
add_filter( 'list_terms_exclusions', 'maks_filter_terms', 10, 2 );

// Убрать одиночный пост с главной страницы
function removeFromHome($query) {
    if ($query->is_home)
    {$query->set('cat','-3, -5, -6');}
    return $query; }
add_filter('pre_get_posts','removeFromHome');

// Pagination blog page
function custom_pagination($numpages = '', $pagerange = '', $paged='') {

    if (empty($pagerange)) {
        $pagerange = 2;
    }

    /**
     * This first part of our function is a fallback
     * for custom pagination inside a regular loop that
     * uses the global $paged and global $wp_query variables.
     *
     * It's good because we can now override default pagination
     * in our theme, and use this function in default quries
     * and custom queries.
     */
    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }
    if ($numpages == '') {
        global $wp_query;
        $numpages = $wp_query->max_num_pages;
        if(!$numpages) {
            $numpages = 1;
        }
    }

    /**
     * We construct the pagination arguments to enter into our paginate_links
     * function.
     */
    $pagination_args = array(
        'base'            => get_pagenum_link(1) . '%_%',
        'format'          => 'page/%#%',
        'total'           => $numpages,
        'current'         => $paged,
        'show_all'        => False,
        'end_size'        => 1,
        'mid_size'        => $pagerange,
        'prev_next'       => True,
        'prev_text'       => __('&laquo;'),
        'next_text'       => __('&raquo;'),
        'type'            => 'plain',
        'add_args'        => false,
        'add_fragment'    => ''
    );

    $paginate_links = paginate_links($pagination_args);

    if ($paginate_links) {
        echo "<nav class='custom-pagination'>";
        echo $paginate_links;
        echo "</nav>";
    }

}


?>

